#!/bin/sh

invoice_nr=$1
pdflatex $invoice_nr.tex
rm -f $invoice_nr.log
rm -f $invoice_nr.aux
echo "Done"
